package com.pestamos.rafaeltaveras.srradio;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private ProgressDialog progressDialog=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
      try{
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {// si hay conexion
            Cargarweb("http://147.135.5.58:9882/");
        } else {

            mWebView.setVisibility(View.INVISIBLE);
           Toast.makeText(this, R.string.ErrorConeccion, Toast.LENGTH_SHORT).show();

        }
      }
        catch (Exception e){
          Toast.makeText(this,R.string.ErrorConeccion, Toast.LENGTH_SHORT).show();

      }


        /*DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);*/





    }




    WebView mWebView;
    String miurllocal;
    //Boolean mostrando=false;
    Integer contador=0;

    private  ProgressDialog pd=null;

    private void Cargarweb(String uri) {


        mWebView = (WebView) findViewById(R.id.pagina);

        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new WebViewClient());
        registerForContextMenu(mWebView);
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.setOverScrollMode(WebView.OVER_SCROLL_NEVER);
        registerForContextMenu(mWebView);
        pd=new ProgressDialog(MainActivity.this);
        pd.setTitle(R.string.cargar_web);
        pd.setMessage( "Pleaes wait...");
        pd.setIcon(R.drawable.logo);
        miurllocal=uri;
        mWebView.loadUrl(miurllocal);



        mWebView.setWebViewClient(new WebViewClient() {

            // This method will be triggered when the Page Started Loading

            public void onPageStarted(WebView view, String url, Bitmap favicon) {

                pd.show();
                pd.setCancelable(true);

                super.onPageStarted(view, url, favicon);
            }

            // This method will be triggered when the Page loading is completed

            public void onPageFinished(WebView view, String url) {

                super.onPageFinished(view, url);
             //   mWebView.getSettings().setJavaScriptEnabled(true);

                mWebView.loadUrl(
                        "javascript:(function() { " +
                                "let audio = document.querySelector('audio');\n" +
                                "document.querySelector('font').style.display = 'none';\n" +
                                "document.querySelector('body').style.backgroundColor = 'white';\n" +
                                "document.querySelector('body').appendChild(audio);\n" +
                                "})()");

                if(pd.isShowing()){

                    pd.dismiss();
                    pd.cancel();
                }

                super.onPageFinished(view, url);
            }

            // This method will be triggered when error page appear

            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                pd.dismiss();
                // You can redirect to your own page instead getting the default
                // error page
                Toast.makeText(MainActivity.this,
                        "Internet Connetion error", Toast.LENGTH_LONG).show();
                mWebView.setVisibility(View.INVISIBLE);
                super.onReceivedError(view, errorCode, description, failingUrl);
            }



        });




    }

    @Override
    public void onBackPressed() {
        if (mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            super.onBackPressed();
        }}
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.main, menu);
        getMenuInflater().inflate(R.menu.botonesbarra, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();



        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id==R.id.minimizarapp){
         Toast.makeText(this, "On background playing!", Toast.LENGTH_LONG).show();


            moveTaskToBack(true);

        }

        if (id==R.id.apagarapp){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Confirm");
            builder.setMessage("Exit?");

            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    System.exit(0);
                    dialog.dismiss();
                }
            });

            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    // Do nothing
                    dialog.dismiss();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();



        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void salirAccion(View view) {
        System.exit(0);
    }

    public void minimizarAccion(View view) {

        Toast.makeText(this, "On background playing!", Toast.LENGTH_SHORT).show();
        if (mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            super.onBackPressed();
        }}




}
